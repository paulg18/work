﻿namespace filesSearch
{
    partial class FormFileSearch
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFileSearch));
            this.labelNameFile = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.textBoxTextInFile = new System.Windows.Forms.TextBox();
            this.labelTextInFile = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.labelDirectoryselection = new System.Windows.Forms.Label();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.textBoxSelectFolder = new System.Windows.Forms.TextBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.labelShow = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.labelTime = new System.Windows.Forms.Label();
            this.labelTimeShow = new System.Windows.Forms.Label();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelNameFile
            // 
            this.labelNameFile.AutoSize = true;
            this.labelNameFile.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelNameFile.Location = new System.Drawing.Point(15, 14);
            this.labelNameFile.Name = "labelNameFile";
            this.labelNameFile.Size = new System.Drawing.Size(73, 13);
            this.labelNameFile.TabIndex = 0;
            this.labelNameFile.Text = "File name: ";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.textBoxFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFileName.Location = new System.Drawing.Point(94, 12);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(135, 20);
            this.textBoxFileName.TabIndex = 1;
            // 
            // textBoxTextInFile
            // 
            this.textBoxTextInFile.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.textBoxTextInFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTextInFile.Location = new System.Drawing.Point(318, 12);
            this.textBoxTextInFile.Name = "textBoxTextInFile";
            this.textBoxTextInFile.Size = new System.Drawing.Size(139, 20);
            this.textBoxTextInFile.TabIndex = 3;
            // 
            // labelTextInFile
            // 
            this.labelTextInFile.AutoSize = true;
            this.labelTextInFile.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelTextInFile.Location = new System.Drawing.Point(235, 15);
            this.labelTextInFile.Name = "labelTextInFile";
            this.labelTextInFile.Size = new System.Drawing.Size(91, 13);
            this.labelTextInFile.TabIndex = 2;
            this.labelTextInFile.Text = "Text in file: ";
            // 
            // buttonSearch
            // 
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSearch.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearch.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonSearch.Location = new System.Drawing.Point(241, 81);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(71, 44);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // labelDirectoryselection
            // 
            this.labelDirectoryselection.AutoSize = true;
            this.labelDirectoryselection.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDirectoryselection.Location = new System.Drawing.Point(15, 57);
            this.labelDirectoryselection.Name = "labelDirectoryselection";
            this.labelDirectoryselection.Size = new System.Drawing.Size(133, 13);
            this.labelDirectoryselection.TabIndex = 5;
            this.labelDirectoryselection.Text = "Directory selection: ";
            // 
            // buttonSelect
            // 
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonSelect.Location = new System.Drawing.Point(423, 52);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(34, 23);
            this.buttonSelect.TabIndex = 6;
            this.buttonSelect.Text = "...";
            this.buttonSelect.UseVisualStyleBackColor = true;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // textBoxSelectFolder
            // 
            this.textBoxSelectFolder.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.textBoxSelectFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSelectFolder.Location = new System.Drawing.Point(154, 55);
            this.textBoxSelectFolder.Name = "textBoxSelectFolder";
            this.textBoxSelectFolder.Size = new System.Drawing.Size(257, 20);
            this.textBoxSelectFolder.TabIndex = 7;
            // 
            // treeView
            // 
            this.treeView.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeView.Location = new System.Drawing.Point(12, 81);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(202, 326);
            this.treeView.TabIndex = 10;
            // 
            // labelShow
            // 
            this.labelShow.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelShow.Location = new System.Drawing.Point(12, 414);
            this.labelShow.Name = "labelShow";
            this.labelShow.Size = new System.Drawing.Size(448, 13);
            this.labelShow.TabIndex = 11;
            this.labelShow.Text = "Search stop";
            // 
            // timer
            // 
            this.timer.Interval = 950;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // labelTime
            // 
            this.labelTime.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTime.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelTime.Location = new System.Drawing.Point(231, 360);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(56, 15);
            this.labelTime.TabIndex = 12;
            this.labelTime.Text = "Timer: ";
            // 
            // labelTimeShow
            // 
            this.labelTimeShow.AutoSize = true;
            this.labelTimeShow.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTimeShow.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelTimeShow.Location = new System.Drawing.Point(293, 359);
            this.labelTimeShow.Name = "labelTimeShow";
            this.labelTimeShow.Size = new System.Drawing.Size(72, 18);
            this.labelTimeShow.TabIndex = 13;
            this.labelTimeShow.Text = "00:00:00";
            // 
            // buttonStop
            // 
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonStop.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStop.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonStop.Location = new System.Drawing.Point(318, 81);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(63, 44);
            this.buttonStop.TabIndex = 14;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPause.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPause.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonPause.Location = new System.Drawing.Point(387, 81);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(70, 44);
            this.buttonPause.TabIndex = 15;
            this.buttonPause.Text = "Pause";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClose.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClose.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonClose.Location = new System.Drawing.Point(371, 340);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(89, 54);
            this.buttonClose.TabIndex = 16;
            this.buttonClose.Text = "Close program";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // FormFileSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(472, 439);
            this.ControlBox = false;
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.labelTimeShow);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelShow);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.textBoxSelectFolder);
            this.Controls.Add(this.buttonSelect);
            this.Controls.Add(this.labelDirectoryselection);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxTextInFile);
            this.Controls.Add(this.labelTextInFile);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.labelNameFile);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(488, 473);
            this.MinimumSize = new System.Drawing.Size(488, 473);
            this.Name = "FormFileSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Files Search";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelNameFile;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.TextBox textBoxTextInFile;
        private System.Windows.Forms.Label labelTextInFile;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label labelDirectoryselection;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TextBox textBoxSelectFolder;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Label labelShow;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelTimeShow;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonClose;
    }
}

