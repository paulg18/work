﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace filesSearch
{
    public partial class FormFileSearch : Form
    {
        
        static string tmpDir = "";
        static string tmpFileName = "";
        static string tmpTextFile = "";
        string tmpPath = "";
        bool paused = true;
        DateTime start;
        TreeNode curNode;
        Thread myThread;


        public FormFileSearch()
        {
            InitializeComponent();
            DownloadValues();
            InitElem();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            fbd.Description = "Select directory...";
            fbd.ShowNewFolderButton = false;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                tmpDir = fbd.SelectedPath + "\\";
                textBoxSelectFolder.Text = tmpDir;
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            tmpDir = textBoxSelectFolder.Text;
            tmpFileName = textBoxFileName.Text;
            tmpTextFile = textBoxTextInFile.Text;

            if (paused)
                StartTimer();
            else
                StopTimer();
            myThread = new Thread(new ParameterizedThreadStart(SearchFile));
            myThread.Start(tmpDir);
            StopTimer();

        }

        private void StartTimer()
        {
            paused = false;
            start = DateTime.Now;
            labelShow.Text = "00:00:00";
            timer.Enabled = true;
        }

        private void StopTimer()
        {
            paused = true;
            timer.Enabled = false;
            labelTimeShow.Text = "00:00:00";
        }

        private void SearchFile(object folderPath)
        {
            string[] allFiles = Directory.GetFiles(folderPath.ToString(), tmpFileName + ".txt", SearchOption.AllDirectories);
            foreach(string file in allFiles)
            {
                if (InvokeRequired)
                    this.Invoke(new Action(() => labelShow.Text = file));
                else
                    labelShow.Text = file;
                string tmp = File.ReadAllText(file);
                if (tmp.IndexOf(tmpTextFile, StringComparison.CurrentCulture) != -1)
                {
                    tmpPath = file.Remove(file.LastIndexOf('\\'));
                    InitFolder(tmpDir);
                }
            }

        }

        private void InitFolder(string path)
        {
            if (InvokeRequired)
                this.Invoke(new Action(() => treeView.BeginUpdate()));
            else
                treeView.BeginUpdate(); 
            string[] root = Directory.GetDirectories(path);
            DirectoryInfo di;
            foreach (string s in root)
            {
                di = new DirectoryInfo(s);
                BuildTreeAsync(di, treeView.Nodes);
            }
            if (InvokeRequired)
                this.Invoke(new Action(() => treeView.EndUpdate()));
            else
                treeView.EndUpdate();
        }

        private void InitElem()
        {
            textBoxFileName.Text = tmpFileName;
            textBoxSelectFolder.Text = Properties.Settings.Default.dir;
            textBoxTextInFile.Text = Properties.Settings.Default.textFile;
        }

        private void SaveValues()
        {
            Properties.Settings.Default.dir = textBoxSelectFolder.Text; ;
            Properties.Settings.Default.fileName = tmpFileName;
            Properties.Settings.Default.textFile = tmpTextFile;
            Properties.Settings.Default.Save();
        }

        private void DownloadValues()
        {
            tmpDir = filesSearch.Properties.Settings.Default.dir;
            tmpFileName = filesSearch.Properties.Settings.Default.fileName;
            tmpTextFile = filesSearch.Properties.Settings.Default.textFile;
        }

        private async void  BuildTreeAsync(DirectoryInfo di, TreeNodeCollection nodes)
        {
            if (InvokeRequired)
                this.Invoke(new Action(() => curNode = nodes.Add("Folder", di.Name)));
            else
                curNode = nodes.Add("Folder", di.Name);
            foreach(DirectoryInfo subdir in di.GetDirectories())
            {
                await Task.Run(()=> BuildTreeAsync(subdir, nodes));
            }

            foreach(FileInfo fi in di.GetFiles())
            {
                if(tmpFileName + ".txt" == fi.Name)
                    if (InvokeRequired)
                        this.Invoke(new Action(() => curNode.Nodes.Add(fi.Name)));
                    else
                        curNode.Nodes.Add(fi.Name);
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            SaveValues();
            this.Close();
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            StopTimer();
            buttonPause.Enabled = false;
            myThread.Suspend();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            treeView.Nodes.Clear();
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (paused)
                return;
            TimeSpan time = (DateTime.Now - start).Duration();
            labelTimeShow.Text = time.ToString(@"hh\:mm\:ss");
        }
    }
}
